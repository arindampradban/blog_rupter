---
layout: post
title: A visit to IIT BBSR
location: Bhubaneswar
tags:
- places
---

Today I had fun exploring **[IIT Bhubaneswar Campus ](http://www.iitbbs.ac.in/) **.I am finding my own ways to interact with new people and trying to get *involved* in some research opportunities.Yup,I also have some interest in *r&d* and I have approached a professor so that I can get a strong hold on my *research background*.From my current knowledge opensourcing is mostly about application ,implementation and improvement of technology.Research relates to possibility of technology<!--excerpt--> in wide area, journal and conference stuffs.But these two things are primarily based on the same essence of loving what you do and have lot of things in common in terms of fun with what you create and the possibility of how it serve others.

>Back to my thought about the campus,the IIT Bhubaneswar Campus is primarily based on **Toshali Plaza** ,Gajapati Nagar.It's a nine storied **huge building** (*in my town that's considered big*). Most of the lab and research for PhD and M.Tech graduates are held there.Here's the view...

![toshali](/assets/3_1.png)  
**Source** from [wikimapia](http://photos.wikimapia.org/p/00/02/35/95/34_big.jpg).

>And the Campus has also a **kharagpur extension**,most of the classes and labs currently held there are for the B.tech undergraduate students(mostly core engineers).Most of the **workshops** are there.All the building are developing soon and there a number of tenders currently running.

![kgp_extns](/assets/3_2.png)

I will post my activities *soon*.
