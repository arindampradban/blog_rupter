---
layout: post
title: Pycon India
location: Bangalore
tags:
- places
- events
---

![pycon](/assets/pycon1.jpg) 

This year I attended pycon india,I was eager to meet some of the greatest minds in the opensource world.
The keynote started with [Kushal Das](https://twitter.com/kushaldas)...and he shared some of his wonderful experiences with python.How he began his journey in his undergraduate years and how his projects helped the local community educate using computers.
He has also been core contributer to a wide variety of upstream projects.
<!--excerpt-->

> **Upstream** a powerful word I came across.

My schedule
===========

It's was confusing to decide which talk to attend because this time they had **[parallel talks](http://in.pycon.org/2014/schedule.html)** running on 2 auditorium.So I had to always choose ..They all seem very interesting and most of them were 45min talks.

Anyways,I decided to go to:

* [Python + Spark: Lightning Fast Cluster Computing ](http://in.pycon.org/funnel/2014/108-python-spark-lightning-fast-cluster-computing)
 (Jyotiska NK)

* [Django Design Patterns](http://in.pycon.org/funnel/2014/227-django-design-patterns) (Arun Ravindran)

* [Messing with government data using Python](http://in.pycon.org/funnel/2014/207-messing-with-government-data-using-python) (Anand Chitipothu)

* Ya the lunch was great too.

* [Meet ElasticSearch - My Pet Crunch Monster ](http://in.pycon.org/funnel/2014/253-meet-elasticsearch-my-pet-crunch-monster-directi) (Dhananjay Sathe)

* [GPU accelerated high performance computing primer
](http://in.pycon.org/funnel/2014/158-gpu-accelerated-high-performance-computing-primer) (Gurajapu Raja Suman)

>The most fascinating talk for me was *Messing with government data using Python*
*- Anand Chitipothu*

I have a *huge interest in datamining*.And I got to know about small hacks.

Mostly I used *beautifulsoup and requests...*
Now I am going to give a shot to scrapy and mechanize..
They seem to differ as they can trigger things at runtime.

The talk for elastic search was good too.I am a newbie at database stuffs.But still good to attend some new stuffs.


My schedule for next day :
===========================

The keynote for next day started with [voidspace](https://twitter.com/voidspace).

![pycon](/assets/pycon3.png) 
>He talked about [go lang](https://golang.org/) and his startup juju.And how go error handling differs from others.I also got a chance to talk to him  and asked about opensource projects.He adviced me to find for junior jobs on irc channels and see if I can do them.


So my schedule was exciting:

* [Async IO for Dummies](http://in.pycon.org/funnel/2014/186-asyncio-for-dummies) (Vajrasky Kok)

* [Building highly scalable web-services with Gevent - experiences at Plivo
](http://in.pycon.org/funnel/2014/248-building-highly-scalable-web-services-with-gevent-experiences-at-plivo) (Aditya Manthramurthy)

* [Faster data processing in Python](http://in.pycon.org/funnel/2014/165-faster-data-processing-in-python) (Anand S)


* [Graph everything with Neo4J and Python](http://in.pycon.org/funnel/2014/252-neo4j-and-python-playing-with-graph-data) (Sonal Raj)

>I loved this day...I loved Faster data processing in Python 
*- Anand S*

The simplicity of the talk was 1.It was a benchmark topic for me.

He not only showed ways ,but python libraries to use for faster data processing  and also urged people to think whether they need the speed or not.

He started with a simple program that reads a csv file and formats the required data for votes in a state in particular year.He explored the data stories visually with Python and Javascript.

>He optimized his code as fluent as one can get in algorithms.

He started wth a basic python code to solve the problem ,later he used cpython then pypy @jit to optimize the code.It stared with a runtime of 1.5 millisec in standard python to microsec in cpython and to nanosec in [@jit](http://pypy.org/).


And yes how can I miss to mention this 9yr prodigy who programmes an arduino.

![pycon](/assets/pycon2.png)

Things were really amazing.
I will update soon.